/*application router*/
(function(){

var route = angular.module('appRouter', ['ui.router']);

route.config(function($stateProvider){
	$stateProvider.state('/',{})
				  .state('signin',{
					templateUrl: 'templates/login/login.html',
					controller : 'LoginController',
					controllerAs : 'appLogin'
				})
				.state('registration', {
				
					templateUrl : 'templates/register/registration.html',
					controller : 'RegistrationController',
					controllerAs : 'appRegister'
					
				}).state('koolets',{
					
					templateUrl : 'templates/koolets_modules/koolets_module.html',
					controller : 'KooletsModController',
					controllerAs : 'kooletsMods'
					
				}).state('koolets.discovery',{
				
					templateUrl : 'templates/koolets_modules/discovery/discovery.html'
				
				}).state('koolets.events',{
				
					template : '<h1>Events</h1>'
					
				}).state('koolets.messages',{
				
					template : '<h1>Messages</h1>'
				
				}).state('koolets.pods',{
				
					templateUrl : 'templates/koolets_modules/pods/pods.html',
					controller: 'PodsController',
					controllerAs : 'appPods'
				
				}).state('koolets.profile',{
				
					templateUrl : 'templates/koolets_modules/profile/profile.html',
					controller : 'ProfileController',
					
				
				});
		});

})()