/* koolets team 
* project start year 2015
* koolets copyright 
*/

/* This serves as the base module 
*  which can be use or share 
*  by other modules 
*/
(function(){

var app = angular.module('app',['koolets', 'appRouter', 'ngMaterial','kooletsLogin','kooletsRegister', 'ngMessages','LocalStorageModule', 'KooletsMod', 'Profile', 'Pods','PodsService']);

app.config(function($mdThemingProvider, localStorageServiceProvider){
	
	localStorageServiceProvider.setStorageType('localStorage');

});

app.constant("baseUrl", 'http://ec2-52-74-230-113.ap-southeast-1.compute.amazonaws.com:');

//configKoolets.value("portEvent", '9011/'); //event koolets_event.js
//configKoolets.value("portLocation", '9003/'); //event koolets_location.js
app.constant("portPods", '9020/') ;//event koolets_pods9011.js
//configKoolets.value("portUtils", '9100/'); //event koolets_utils.js
app.constant("portUser", '9001/'); //event koolets.js


app.constant('userCreate', 'createKooletsUser');
app.constant('userLogin', 'login');
//configKoolets.value('userUploadPic', 'uploadProfile_Pics');
app.constant('userDetails', 'getKooletsUser/');
//configKoolets.value('userUpdate', 'updateKooletsUser/');
//configKoolets.value('userCheckPassword', 'checkCurrentPassword/');
app.constant('userPic', 'getUserProfilePic/');
//configKoolets.value('userSearch', 'kooletsUser/search/');
//configKoolets.value('userQR', 'getUserQRCode/')
	
	
	
	
/**pods routes**/
//configKoolets.value("InsertPods", 'pods');
app.constant("AllPods", 'pods/public/');
//configKoolets.value('PodBannerUpload', 'podsBanner/');
app.constant('PodPicture', 'podsBanner/');
//configKoolets.value('PodID', 'pods/');
//configKoolets.value('PodJoin', 'pod/join'); //note . POST/PUT**
//configKoolets.value('PodGetJoin', 'pod/join/') //note. GET**
//configKoolets.value('PodMembers', 'pod/members/') // GET members
//configKoolets.value('PodUnjoin', 'pod/join/') //Delete or Unjoin 
/**end*/

	
var date = [
			{"val":01,"day" : 1},
			{"val":02,"day" : 2},
			{"val":03,"day" : 3},
			{"val":04,"day" : 4},
			{"val":05,"day" : 5},
			{"val":06,"day" : 6},
			{"val":07,"day" : 7},
			{"val":08,"day" : 8},
			{"val":09,"day" : 9},
			{"val":10,"day" : 10},
			{"val":11,"day" : 11},
			{"val":12,"day" : 12},
			{"val":13,"day" : 13},
			{"val":14,"day" : 14},
			{"val":15,"day" : 15},
			{"val":16,"day" : 16},
			{"val":17,"day" : 17},
			{"val":18,"day" : 18},
			{"val":19,"day" : 19},
			{"val":20,"day" : 20},
			{"val":21,"day" : 21},
			{"val":22,"day" : 22},
			{"val":23,"day" : 23},
			{"val":24,"day" : 24},
			{"val":25,"day" : 25},
			{"val":26,"day" : 26},
			{"val":27,"day" : 27},
			{"val":28,"day" : 28},
			{"val":29,"day" : 29},
			{"val":30,"day" : 30},
			{"val":31,"day" : 31}];
var month = [
						{"val" : 01, "mnth": "January"},
						{"val" : 02, "mnth": "February"},
						{"val" : 03, "mnth": "March"},
						{"val" : 04, "mnth": "April"},
						{"val" : 05, "mnth": "May"},
						{"val" : 06, "mnth": "June"},
						{"val" : 07, "mnth": "July"},
						{"val" : 08, "mnth": "August"},
						{"val" : 09, "mnth": "September"},
						{"val" : 10, "mnth": "October"},
						{"val" : 11, "mnth": "November"},
						{"val" : 12, "mnth": "December"},
					];
					
var year = [ 2015,2014,2013,2012,2011,2010,2009,2008,2007,2006,2005, 2004,2003,2002,2001,2000,1999,1998,1997,1996,1995,
			 1994,1993,1992,1991, 1990, 1989, 
			 1988,1987,1986,1985, 1984, 1983, 1982, 1981, 1980,  1979, 1978, 1977, 1976, 1975, 1974, 1973,1972, 1971, 1970	
		   ];		
				
app.constant('Date', date);	
app.constant('Month', month);
app.constant('Year', year);
	

	
/**
* clear local storage login user data
* delays 100mseconds to change state
*/
app.factory('ClearLocalStorage', function(localStorageService, $state, $timeout){
	return{
	
		clearUserLoginData : function(){
		
		localStorageService.clearAll(); //empty localstorage
		
		
			$timeout(function(){
			
				$state.go('signin');
			
			},100)
		}
		
	}
});

	
/**
* this service factory saves the users login data token, user id, lastsign-in
* @params - _user_login is an object containing token and user id and last sign in date
* this is inject into the login.js
*/	
app.factory('UserCredentials', function(localStorageService){
	
		var usersLoginData = {};
		
		usersLoginData.setUserData = function(_user_login){
		
			localStorageService.set('koolets_user', _user_login.koolets_user);
			localStorageService.set('koolets_token',  _user_login.koolets_token);
			localStorageService.set('last_signin', _user_login.last_signin);
		}
		
		return usersLoginData;
	});
	

/**
* checks if the user is logged in
*/
app.factory('LoggedIn',function(localStorageService, $log, $state){
	
	var UserLoggedIn = {};
	
	UserLoggedIn.isUserLoggedIn = function(){
		this.checkKooletsToken();
	}
	//check localStorage if values has been set
	UserLoggedIn.checkKooletsToken = function(){
		
		if(localStorageService.get('koolets_user') == null){
			$log.info("user is null" )
			$state.go('signin');
		}else{
			this.checkKooletsUser();
		}
	}
	
	UserLoggedIn.checkKooletsUser = function(){
		if(localStorageService.get('koolets_token') == null){
			$log.info('koolets_token is null');
			
			$state.go('signin');
			
		}else{
			this.checkKooletsLastSignIn();
		}
	}
	
	UserLoggedIn.checkKooletsLastSignIn = function(){
		if(localStorageService.get('last_signin') == null){
			$log.info('last_signin is null');
			
			$state.go('signin');
			
		}else{
		
			$state.go('koolets');
		}
	}
	
	return UserLoggedIn;
});
	
	
 /**
 * this service gets the user profile picture
 * baseUrl, portUser, and userPic are constant service
 * which is injected to the custom service
 *
 * @param -baseUrl the main url e.g http://your_url.com.service
 * @param -portUser the default port user e.g 8080 
 * @param -userPic the main route to get user picture e.g /sample/sample/route
 *
 */
	app.factory('Profile', function($q, $http, $log ,baseUrl, portUser, userPic, $timeout,localStorageService){
	
		
		var factory = {};
		factory.getProfile = function(){
			
		return $http.get(baseUrl + portUser + userPic + localStorageService.get('koolets_user') + '/' + 64).then(function(result){
			
		
				return result.config.url;
		
			
		 },function(){
			return "Cannot get user profile picture.";
		 });
				
		}
		
		return factory;
	});	
	
	
	
 /**
 * this service fetch the user information details  in the server
 *
 */
 app.factory('ProfileDetails', function($http, $log ,baseUrl, portUser, userDetails, localStorageService){
	
		
		var User = {};
		
		
		
		User.getInformationDetails = function(){
		$http.get(baseUrl + portUser + userDetails + localStorageService.get('koolets_user')).then(function(result){
				$log.info("All details", result.data);
				
			});
		};
		//@ user 1333
		User.setUserName = function(_data){
			
			var username = _data.koolets_user[0].username;
			this.UserName = username;
			console.log(username);
			
		};
		
		//@ Pepe, Smith
		User.getFullName = function(){
		
		var Fullname ="";
		
		return	$http.get(baseUrl + portUser + userDetails + localStorageService.get('koolets_user')).then(function(result){
				$log.info("All details", result.data);
					
					return Fullname = result.data.koolets_user[0].fname + ', ' +  result.data.koolets_user[0].lname;
			});
		};
		
		//@ All personal information
		User.getAllDetails = function(_data){
				return $http.get(baseUrl + portUser + userDetails + localStorageService.get('koolets_user')).then(function(result){
			
					return result.data;
				
			});
			
		};
		
	return User;
 });
 
 
 	/**
	*concatenate bithdate in YYYY-MM-DD format
	* and return the result
	* @param year - user birth year
	* @param month - user birth month
	* @param day - user birth day
	*/
	app.factory('BirthDateConcatenate',  function(){
		return{
			combinedBirthdates : function(year, month, day){
				
				var finalBirthdate = year + '-' + month  + '-' + day;
				
				return finalBirthdate;
			}
		}
	});
	
	/**
	*custom service that will split the date 
	* into Year, Month, Day
	*@param _date - raw date format YYYY-MM-DD
	*/
	app.factory('BirthDateSplitter', function(){
	
		var Year;
		var Month;
		var Day;
	
		return {
			splitDates : function(_date){
				var date = _date.split('T');
				
				var YearMonthDay = date[0].split("-");
				
				Year = YearMonthDay[0];
				
				Month = YearMonthDay[1];
				
				Day = YearMonthDay[2];
				
			},
			getYear : function(){
				return Year;
			},
			getMonth : function(){
				
				return Month; 
			},
			getDay : function(){
			
				return Day;
			}
		}
	});
	
})()


