/**
* koolets Front Page/Main Page module controller
*/

(function(){

var koolets = angular.module('koolets',[]);

koolets.controller('MainController', function($log, $state, $scope,LoggedIn){

var self = this;

	$log.info("Main -> MainController is loaded.");

		
		//goes to signin page
		self.signin = function(){
			$state.go('signin');
		}
		//goes to register page
		self.register = function(){
			$state.go('registration');
		}
		
		
	//check if the use is logged in else go to its koolet modules page
	LoggedIn.isUserLoggedIn();
		
});
	
})()