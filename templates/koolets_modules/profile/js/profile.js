(function(){
	var profile = angular.module('Profile',[]);
	
	profile.controller('ProfileController',  function(ProfileDetails,BirthDateSplitter, Year, Month, Date,$scope, $log, $timeout,Profile){

	$scope.saveStateButton = true;
	$scope.year = Year;
	$scope.month = Month;
	$scope.date = Date;
	
	
	//service to get the user profile picture
	Profile.getProfile().then(function(_url){
	
		$scope.URL_PROFILE = _url;
		
	},function(_err){
	
		$log.debug(_err);
		
	});
	
	
	ProfileDetails.getAllDetails().then(function(data){
		
		BirthDateSplitter.splitDates(data.koolets_user[0].birthdate);
	
		//set object FORM property
		$scope.FORM ={
		username : data.koolets_user[0].username,
		firstname : data.koolets_user[0].fname,
		middlename : data.koolets_user[0].mname,
		lastname : data.koolets_user[0].lname,
		gender  : data.koolets_user[0].gender,
		birthYear : parseInt(BirthDateSplitter.getYear()),
		birthDate : parseInt(BirthDateSplitter.getDay()),
		birthMonth : parseInt(BirthDateSplitter.getMonth())
		}
		
		$scope.cloneForm(); //call function
	});
	
	// creates a shallow copy of the FORM object
	// in case the user clears an input fields it will
	// return its original data
	$scope.cloneForm = function(){
		$scope.FORMCopy = angular.copy($scope.FORM);
		console.log($scope.FORMCopy, "Copy");
	}

	$scope.$watchCollection('FORM',  function(){
	
		
		console.log("Hey i got changed.");  
		
	});
	
	
	});
})()