(function(){
	var pods = angular.module('PodsService', []);

	
	/*fetch all public pods available for user
	*@param result - is the result coming from the database
	*/
	pods.factory('PublicPods', function(baseUrl, portPods, AllPods,localStorageService, $log, $http){
		return{
			getPublicPod: function(){
				return $http.get(baseUrl + portPods + AllPods + localStorageService.get('koolets_user')).then(function(result){
				
					return result.data;
				});
			}
		}
	});
	
	
	
	/* url provider for getting the pod banner
	* returns url
	*/
	pods.factory('PodBanner',  function(PodPicture, portPods, PodPicture, baseUrl){
		return{
			getBannerUrl : function(){
				
				var url = baseUrl + portPods + PodPicture;
				return url;
			}
		}
	});
})()