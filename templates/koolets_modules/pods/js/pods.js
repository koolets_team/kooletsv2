/*
*PodsService module is used here
*/
(function(){
	var pods = angular.module('Pods', ['PodsService']);
	
	pods.controller('PodsController', function(PublicPods, PodBanner,$log){
		
		var self = this;
		
		console.log("Pods controller is loaded");
		
		PublicPods.getPublicPod().then(function(data){
			$log.info("data", data);
			self.pods = data;
		});
		
		
		$log.info(PodBanner.getBannerUrl());
		
		self.urlBanner = PodBanner.getBannerUrl();
		
	});
	
	
	
})()