(function(){
	var koolets_mod = angular.module('KooletsMod',[]);
	
	koolets_mod.controller('KooletsModController', function($log, $mdBottomSheet, $state, $mdSidenav, ClearLocalStorage, Profile,ProfileDetails){
	
	var self = this;
	
	self.discovery = function(){
		$state.go('koolets.discovery');
	}
	
	self.events = function(){
		$log.info("I got events")
		$state.go('koolets.events');
	}
	
	
	self.message = function(){
		$log.info("I got message")
		$state.go('koolets.messages');
	}
	
	self.pods = function(){
		$log.info("I got pods")
		$state.go('koolets.pods');
	}
	
	
	self.showLeftMenu = function(){
		$log.info("I got left menu");
		$mdSidenav('left-menu').toggle();
	}
	
	self.logout = function(){
	
		ClearLocalStorage.clearUserLoginData();
	}
	
	
	
	
	self.profile = function(){
	
		$state.go('koolets.profile');
		
	}
	
	//service to get the user profile picture
	Profile.getProfile().then(function(_url){
	
		self.URL_PROFILE = _url;
		
	},function(_err){
	
		$log.debug(_err);
		
	});
	
	ProfileDetails.getInformationDetails();
	
	ProfileDetails.getFullName().then(function(_data){
		self.FullName = _data;
	});
	
	self.showActionSettings = function(_evt){
		$log.info("Show action settings");
		$mdBottomSheet.show({
			templateUrl : 'templates/koolets_modules/action_settings.html',
			controller : function($state){
				var self = this;
				self.profile = function(){
					$state.go('koolets.profile');
					$mdBottomSheet.hide();
				}
			},
			controllerAs : 'bottomAction'
		});
	}

	});
	
	

	
})()