/**login module and login controller*/

(function(){

var login = angular.module('kooletsLogin',[]);
	
login.controller('LoginController', function($log, $state, Login, UserCredentials, LoggedIn,ProfileDetails){
	$log.info("Login Controller is loaded");	
		
	var self = this;
		
	
	 
	self.register = function(){
		$state.go('registration');
	}
	
	self.login = function(){
		Login.login(self.email, self.password).then(function(data){
		
				if(angular.equals(data.result, "user ok")){
				
					UserCredentials.setUserData(data)//store to local storage
					
				
					
					$state.go('koolets');
					
				}
			
			});
		}
	LoggedIn.isUserLoggedIn();
	
	});
	
	
	
	
/**@param _email and _password
*	
*/
login.factory('Login', function($http, $log, portUser, baseUrl, userLogin){
	
	var LoginService = {};
	
	LoginService.login = function(_email, _password){
		
		var hash = CryptoJS.SHA3(_password, { outputLength: 224 });
		
		var hashPassword = ""+hash;
		var email = _email;
		
		$log.info(hashPassword, email);
		
		
		return $http.post(baseUrl + portUser + userLogin, {"email" : email, "password": hashPassword}).then(function(result){
			$log.info("Login Result", result.data);
			return result.data;
		});
	
	};
	return LoginService;
});
	
})()